# README #

Classification damaged cars. Description

### Requirements ###

* python 3.6
* GPU with CUDA (preferably)
* installed Tensorflow

### Tensorflow installation guide ###

* https://alliseesolutions.wordpress.com/2016/09/08/install-gpu-tensorflow-from-sources-w-ubuntu-16-04-and-cuda-8-0/
* http://simonboehm.com/tech/2017/06/23/installingTensorFlow.html

### How to start ###

* apt-get install -y python3-pip python3-dev build-essential python3-venv
* python3 -m venv
* source venv/bin/activate
* pip install -r requirements.txt

### Scrapping data ###

* damaged cars : https://www.iaai.com/
* clear cars : http://ai.stanford.edu/~jkrause/cars/car_dataset.html

### Notebook for demo ###

* car_project.ipynb

### data ###

* data_car.rar - unzip to folder skin_disorders/data_car

* https://drive.google.com/open?id=14VcobcM17jhSBDaBpDpGun3GEoBt-G73

